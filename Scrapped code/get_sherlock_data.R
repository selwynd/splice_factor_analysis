get_certain_splice_factors <- function(wd.data){
  SF.INPUT.CERTAIN <- read.csv(paste0(wd.data, "certain_splice_factors.csv"), sep="\t", header=T)
  return(SF.INPUT.CERTAIN)
}


get_splice_factors <- function(wd.data, threshold=0){
  SF.INPUT <- read.csv(paste0(wd.data, "uniprot.csv"), sep="\t", header=T)
  SF.OUT <- SF.INPUT[SF.INPUT$total_score >= threshold,]
  SF.OUT$ENSG <- as.character(SF.OUT$ENSG)
  return(SF.OUT)
}

get_gene_expression <- function(wd.gene, brush){
  raw <- read.table(paste0(wd.gene, brush,"/gene_expression.txt"), header=T, row.names = 1)
  filter <- filterByExpr(DGEList(raw))
  filtered <- raw[filter,]
  return(list(filtered, raw))
}

get_clinical_data <- function(wd.data, brush){
  #Read the clinical data file in and select only the age and pack.year columns 
  Clinical.data <- read.table(paste0(wd.data, "Clinical_",brush,"_sherlock.txt"), header=T, sep="\t")
  return(Clinical.data)
}

get_rmats_paired <- function(brush){
  
  SJ.out.files <- Sys.glob(file.path(wd.rmats, "sherlock/",paste0(brush,"*")))
  
  for (name in SJ.out.files) {
    file_name <- str_extract(name, "(?<=_)[^/]*(?=.JCEC)")
    print(file_name)
    
    temp <- read.csv(name, header=T, sep = "\t")
    assign(paste0(brush,".AS.", file_name, ".PAIRED"), temp, envir = parent.frame())
  }
  temp <- read.csv(paste0(wd.rmats, "sherlock/","model_", brush, ".txt"), header=T, sep = "\t")
  assign(paste0(brush,".PAIRED.MODEL"), temp, envir = parent.frame())
  
  
}