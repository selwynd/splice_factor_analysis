
get_DE_vars <- function(df.rna, df.clin, model.txt){
  
  out.dge <- DGEList(counts=df.rna)

  model <- model.matrix(model.txt)
  
  for(co in colnames(model)[-1]){
      if(!exists(paste0("A.GENE.DE.", co))){
        
        disp <- estimateDisp(out.dge, model, robust = TRUE)
        fit <- glmQLFit(disp)
        fit.qlf <- glmQLFTest(fit, coef =co)
        y <- topTags(fit.qlf,n=nrow(fit.qlf$table), adjust.method="BH", p.value=1)
        assign(paste0("A.GENE.DE.", co), y, envir = parent.frame())
      }
    }
}

get_DE_psi <- function(model.txt, clinical){
  
  model <- model.matrix(model.txt)
  event_types <- c("A3SS", "A5SS", "MXE", "SE","RI")
  
  for(co in colnames(model)[-1]){
      for (event in event_types) {
        if(!exists(paste0("A.DE.PSI.",event, ".",co))){
          input <- get(paste0("A.AS.", event,".PSI")) %>%
            dplyr::select(ID,rownames(clinical))
          rownames(input) <- input$ID
          
          input[is.na(input)] <- 0 
          input[,-1] <- input[,-1] *100
          
          out.dge <- DGEList(counts=input[,-1])
          
          disp <- estimateDisp(out.dge, model, robust = TRUE)
          fit <- glmQLFit(disp, model, robust=TRUE)
          fit.qlf <- glmQLFTest(fit, coef =co)
          
          y <- topTags(fit.qlf,n=Inf, adjust.method="BH", p.value=1)
          print(paste0(co, " done for ", event))
          
          assign(paste0("A.DE.PSI.",event, ".",co), y, envir = parent.frame())
        }
      }
    }
  
}


make_DE_var_volcano <- function(input.sf, model.txt, wd.images){
  
  model <- model.matrix(model.txt)
  for(co in colnames(model)[-1]){
    data <- get(paste0("A.GENE.DE.", co))
    dataframe <- data$table
  
    data.sf <- na.omit(dataframe[unique(input.sf$ENSG),])
    
    data.signif <- dataframe[-log10(dataframe$FDR) > -log10(0.05),]
    data.sf.signif <- na.omit(data.signif[rownames(data.sf), ])
    
    
   # plt <- ggplot(data = dataframe)+geom_point(aes(logFC, -log10(FDR), alpha=-log10(FDR),size=(-log10(FDR))^2, color=ifelse(-log10(FDR) > -log(0.05),"Significant","Non-significant")))+
  #    geom_point(data = data.sf, aes(logFC, -log10(FDR), alpha=-log10(FDR),size=(-log10(FDR))^2, color="SF Genes"))+
  #    theme(legend.position = "right")+ guides(alpha=FALSE, size=FALSE)+xlab("LogFc (for age in years)")+
  #    geom_hline(yintercept=-log(0.05), linetype="dashed")+labs(caption=paste0("model used: ~", model.txt[2]), subtitle = paste0("Coef selected : ",co))+
  #    ggtitle(paste0("Volcano plot of ", nrow(dataframe), " genes across ", nrow(A.CLINICAL), " samples, with splicing factors highlighted"))+
  #    scale_color_manual(name = "Type", values = c("Significant" = "#cf000f", "Non-significant" = "#446cb3", "SF Genes" = "#2BC039", "Significant SF" ="#913d88"))
    
    
    plt <- ggplot(data = dataframe)+geom_point(aes(logFC, -log10(FDR), alpha=-log10(FDR),size=(-log10(FDR))^2, color=ifelse(-log10(FDR) > -log10(0.05),"Significant","Non-significant")))+
      geom_point(data = data.sf, aes(logFC, -log10(FDR), size=(-log10(FDR))^2, color="SF Genes"))+
      coord_cartesian(ylim=c(0,7))+
      
      theme(legend.position = "right")+ guides(alpha=FALSE, size=FALSE)+xlab("LogFc (for age in years)")+
      geom_hline(yintercept=-log10(0.05), linetype="dashed")+labs(caption=paste0("model used: ~", model.txt[2]), subtitle = paste0("Coef selected : ",co))+
      ggtitle(paste0("Volcano plot of ", nrow(dataframe), " genes across ", nrow(A.CLINICAL), " samples, with splicing factors highlighted"))+
      scale_color_manual(name = "Type", values = c("Significant" = "#be90d4", "Non-significant" = "#446cb3", "SF Genes" = "#29f1c3", "Significant SF" ="#013243"))
    
    
    print(plt)
    
    if(nrow(data.sf.signif) > 1){
      plt <- plt+geom_text_repel(data = data.sf.signif, aes(label = mrt.result$hgnc_symbol[match(row.names(data.sf.signif),mrt.result$ensembl_gene_id)], logFC, -log10(FDR), color="Significant SF"))+
        geom_point(data = data.sf.signif, aes(logFC, -log10(FDR), size=(-log10(FDR))^2, color="Significant SF"))
    }
    print(plt)
    dir.create(paste0(wd.images,"/Atlantis/Gene_expression/Volcano/"), showWarnings = F, recursive = T)
    
    ggsave( paste0(wd.images,"/Atlantis/Gene_expression/Volcano/volcano_plot_model_",co,".png" ),plot=plt, scale = 2)
    }
  
}

make_DE_psi_volcano <- function(model.txt, wd.images){
  
  # model.txt <- A.model.name
  #  co <- "age"
  
  logcpm <- aveLogCPM(A.GENE.EXPR.RAW)
  names(logcpm) <- rownames(A.GENE.EXPR.RAW)
  
  
  model <- model.matrix(model.txt)
  event_types <- c("A3SS", "A5SS", "MXE", "SE","RI")
  
  for(co in colnames(model)[-1]){
    name <- ifelse(co == "pack.year", "smoke", ifelse(co=="FEV1FVCBD", "lung", ifelse(co=="pack.year:FEV1FVCBD", "smoke;lung", "age")))
    if(co == "Age" | co == "age"){
      
      for (event in event_types) {
        input <- get(paste0("A.DE.PSI.",event, ".",co))
        input <- input$table
        
        event.data <- get(paste0("A.AS.",event,".JCEC"))[,1:2]
        input["GeneID"] <- event.data$GeneID[match(rownames(input), event.data$ID)]
        input["GeneExpr"] <- logcpm[match(input$GeneID, names(logcpm))]
        
        
        # plt <- ggplot()+ geom_point(data = input, aes(logFC, -log10(FDR),color=ifelse(-log10(FDR)> -log(0.05), "Significant", "Non-significant"), alpha=-log10(FDR),size=GeneExpr))+
        #  geom_hline(yintercept=-log(0.05), linetype="dashed")+
        #  scale_color_manual(name = "Threshold", values = c("Significant" = "#cf000f", "Non-significant" = "#446cb3"))+
        #  theme(legend.position = "right")+ guides(alpha=F, color = guide_legend(override.aes = list(size=3)))+
        #  ggtitle(paste("Volcano plot of", event, "psi scores per entry", nrow(input) ,"samples"))+
        #  labs(caption=paste0("model used: ~", model.txt[2]), subtitle = paste0("Coef selected : ",co))
        
        
        plt <- ggplot()+ geom_point(data = input, aes(logFC, -log10(FDR),color=GeneExpr, alpha=-log2(FDR),size=GeneExpr* sqrt(-log2(FDR))))+
          geom_hline(yintercept=-log(0.05), linetype="dashed")+
          theme(legend.position = "right")+ guides(alpha=F,size=F, color = guide_legend(override.aes = list(size=4)))+
          ggtitle(paste("Volcano plot of", event, "psi scores per entry", nrow(input) ,"samples"))+
          labs(caption=paste0("model used: ~", model.txt[2]), subtitle = paste0("Coef selected : ",co))+
          scale_color_viridis_c()
        
        dir.create(paste0(wd.images,"/Atlantis/Rmats/PSI/Volcano/",event), showWarnings = F, recursive = T)
        print(plt)
        ggsave(paste0(wd.images,"/Atlantis/Rmats/PSI/Volcano/",event,"/volcano_plot_based_on_",name,".png"),plot=plt, scale =2)
        
      }
    }
  }
}
rmats_pairwise <- function(){
  event_types <- c("A3SS", "A5SS", "MXE", "SE","RI")
  
  for(event in event_types){

    count.incl <- get(paste0("A.RMATS.",event,".INCL")) %>%
      select(!A.remove.sample)
    count.skip <- get(paste0("A.RMATS.",event,".SKIP")) %>%
      select(!A.remove.sample)
    colnames(count.skip) <- paste(colnames(count.skip), ".skipped")
    
    count.both <- cbind(count.incl,count.skip)
    count.both <- count.both[, sort(colnames(count.both))]
    #rownames(count.both) <- get(paste0("K.AS.",event,".JCEC"))$ID
    
    count.psi <- get(paste0("A.AS.",event,".PSI")) %>%
      select(!A.remove.sample)
    
    x <- apply(count.psi, 1, FUN = function(x){ sum(is.na(x))})

    rownames(count.psi) <- count.psi$ID
    count.psi <- count.psi[,-1]
    count.psi["sum"] <- rowSums(count.psi, na.rm = T)
    count.psi["mean"] <- rowMeans(count.psi[,-ncol(count.psi)], na.rm=T)
    
    clin <- as.data.frame(t(A.CLINICAL)) %>%
      select(!A.remove.sample)
    clin <-  as.data.frame(t(clin))
    
    attach(clin)
    
    type <- rep(c("incl", "skip"), ncol(count.incl))
    ages <- rep(Age, each=2)
    sex <- rep(Sex, each=2)
    disease <- rep(Disease, each=2)
    smoking <- rep(Smoking, each=2)
    model.name <- ~ages+type+sex+disease+smoking
    A.model <- model.matrix(model.name)
    
    z1 <- estimateDisp(DGEList(counts = count.both),A.model) %>%
      calcNormFactors(., method="TMM") %>%
      glmQLFit(., A.model) %>%
      glmQLFTest(., coef =2) %>%
      topTags(., n=Inf, adjust.method="BH", p.value=1) %>%
      .$table
    
    z1[6:7] <- count.psi[match(rownames(z1), rownames(count.psi)), (ncol(count.psi)-1):ncol(count.psi)]
    
    pair <- ggplot()+geom_point(data= z1, aes(x = logFC, y = -log10(FDR), color=mean, size=logCPM, alpha=-log10(FDR)))+
      scale_color_gradient2(low = ("red"), high = ("blue"), mid=("purple"), midpoint=0.5, guide="colourbar")+guides(alpha=F, size=F)+
      ggtitle(paste0("Volcano plot of ", event, " splicing type"))+labs(subtitle = paste0(nrow(z1), " splicing events displayed of Atlantis dataset"), caption = paste0("model used = ~",model.name[-1] ))
    
    detach(A.CLINICAL)
    ggsave(paste0(wd.images, "Atlantis/Rmats/", event,"_volcano_age_v2.png"), pair,scale=2)
    
    
    
    assign(paste0("plt.A.", event), pair, envir = parent.frame())
    assign(paste0("PAIRWISE.A.", event), z1, envir = parent.frame())
    
  }
}
SF_heat_and_cor <- function(){
  
  mrt <- useDataset("hsapiens_gene_ensembl", useMart("ensembl"))
  splicing.factor <- getBM(filters="hgnc_symbol", attributes = c("ensembl_gene_id", "hgnc_symbol"), values = splicing.factor.hgnc, mart=mrt)  
  assign("splicing_factor", splicing.factor, envir = parent.frame())
  
  A.sf <- DGEList(get(paste0("A.GENE.EXPR.RAW")))%>%
    calcNormFactors(., method="TMM") %>%
    cpm(.) %>%
    as.data.frame(.) %>%
    t(.) %>%
    as.data.frame(.) %>%
    dplyr::select(matches(splicing.factor$ensembl_gene_id))
  
  colnames(A.sf) <- splicing.factor$hgnc_symbol[match(colnames(A.sf), splicing.factor$ensembl_gene_id)]
  
  
  A.sf.scaled <- as.data.frame(t(scale(A.sf, scale=T, center=T)))
  A.sf.cor <- cor(as.data.frame(t(A.sf.scaled)))
  
  pheatmap(A.sf.scaled, breaks = seq(-4,4, 0.1), main = "Gene expression of splicing factors on Korean", show_colnames=F)
  pheatmap(A.sf.cor, breaks = seq(-1,1, 0.02), main = "Correlation between splicing factor on Korean", show_colnames=F)
  
}

cor_SF_with_events <- function(keep_samples){
  # keep_samples <- K.SAMPLES
  for(event in event_types){
    splice.factor.abundance <- get("A.sf")
    splice.factor.abundance <- splice.factor.abundance[keep_samples,]
    
    psi_score <- get(paste0("A.AS.",event,".PSI"))
    rownames(psi_score) <- psi_score$ID
    psi_score <- psi_score[,-1]
    psi_score <- as.data.frame(t(psi_score))
    
    SF.COR <- cor(psi_score,splice.factor.abundance, use="pairwise")
    SF.COR <- as.data.frame(SF.COR)
    
    pairwise.hits <- get(paste0("PAIRWISE.A.",event))
    heatmap.input <- as.data.frame(t(SF.COR[rownames(head(pairwise.hits, n=100)),]))
    
    plt <- pheatmap(heatmap.input, main = paste0("Correlation of SF with ", event, " events"), breaks = seq(-1,1,0.02), show_colnames = F)
    assign(paste0("SF.heat.", event), plt, envir = parent.frame())
    
    assign(paste0("SF.cor.", event), SF.COR, envir = parent.frame())
  }
  
}
#swarm.col <- list("A3SS" = SF.cor.A3SS$HNRNPH1,"A5SS" = SF.cor.A5SS$HNRNPH1, "MXE" = SF.cor.MXE$HNRNPH1, "RI" = SF.cor.RI$HNRNPH1,"SE" = SF.cor.SE$HNRNPH1)

#boxplot(swarm.col, main="Boxplot of HNRNPH1 correlation on Atlantis", col = 2:6, outline = F, ylab="Correlation", xlab="Event")
#abline(h=0, lty=2, lwd=2)


#SF.heat.A3SS
#SF.heat.A5SS
#SF.heat.MXE
#SF.heat.RI
#SF.heat.SE