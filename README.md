# README #

This repo hosts all the scripts used for finding age/disease related expression paterns in transcriptomic data aswell as finding age/disease related alternative splicing events. This is achieved by implementing differential expression analysis using edgeR and limma. Data is under embargo on request from Groningen Research Institute for Asthma and COPD (GRIAC)

### Dependencies? ###

* Microsoft OpenR 4.0.2
* EdgeR
* Limma

### Script content ###

* Dataset main.R contains the main for running the analysis 
* Dataset_importer.R is imported into the main and retrieves datasets
* Dataset_analyzer contains all the function needed for analysis
* file_downloader a small script used for retrieving data from servers
* Data_infographics contains loose function for making plot of patient parameters

### Who do I talk to? ###

* Selwyn Dijkstra 
* For data retrieval contact prof. dr. Victor Guryev (v.guryev@umcg.nl)